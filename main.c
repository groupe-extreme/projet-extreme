#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "fonction.h"
#define TAILLE_MAX 1000







int main(int argc, char *argv[])

{
	char *tabchar[100];
	int validation;
	int erreur=0;
	img image;
	FILE* inputfichier;
	validation=0;





	for (int i=0; i<argc ;i++)
		{
			tabchar[i]=argv	[i];
			if(strcmp(tabchar[i], "-h") ==0)                //Regarde si l'utilisateur a entré en paramètre un -h
			{
				erreur=1;									// si l'utilisateur a entré un -h alors il sera redirigé vers l'aide
			}
			if(strcmp(tabchar[i], "-i") ==0)				// Regarde si l'utilisateur a entré en paramètre un -i
			{
				validation=validation+1;
			}
		}



	if (argc==1){											//Si l'utilisateur n'a rentré aucun argument il est rédirigé vers l'aide
			erreur=1;
		}


	if (erreur==1)											//Donc si l'utilisateur a rentré en argument -h ou aucun argument il est redirigé vers l'aide
		{
			printf("-------HELP-------\n");
			printf("\n");
			printf("\n");
			printf("-i : definie le fichier d'entrée !OBLIGATOIRE!");
			printf("-o fichier : définie le fichier de sorti\n");
			printf("-b seuil : binarise une image (exemple : ./img -b 175 -i image.ppm -o imageBin.ppm)\n");
			printf("-c : réalise un renforcement de contraste (convolution) (exemple : ./img -c -i image.ppm -o imageContraste.ppm)\n");
			printf("-d : réalise une dilatation (exemple : ./img -d -i image.ppm -o imageDil.ppm)\n");
			printf("-e : réalise une érosion (exemple : ./img -e -i image.ppm -o benderEro.ppm)\n");
			printf("-f : réalise un flou (convolution) (exemple : ./img -f -i image.ppm -o imageFlou.ppm)\n");
			printf("-l : réalise une détection de contours/lignes  (convolution)  (exemple : ./img -l -i image.ppm -o imageContour.ppm)\n");
			printf("-g : converti une image en niveau de gris (exemple : ./img -g -i image.ppm -o imageGris.ppm");
			printf("-r : réalise un recadrage dynamique (exemple : ./img -r -i image.ppm -o imageRecDyn.ppm)\n");
			printf("-h : affiche une aide sur votre programme et quitte le programme avec un code de retour égal à 0\n");
			printf("-n : argument pour créer une image vierge avec un croix au centre avec les propriétés voulu\n");
			printf("-------------------\n");
		}

	else													// Si l'utilisateur a rentré les arguments comme il faut alors on va essayé d'ouvrir son image
		{

			for (int j=0; j<argc ;j++)
			{
				if(strcmp(tabchar[j], "-i") ==0)			// On regarde a quel place il y a le -i
				{
					inputfichier = fopen(argv[j+1],"rb");   // et on récupère le nom du fichier a la place +1 ou est situé le -i car si l'utilsateur a bien fais les choses alors le nom du fichier est après le -i
					if ( inputfichier == NULL )
					{
        				printf( "Cannot open file %s\n", argv[0] );  // Si le fichier transmit n'est pas valable alors on affiche un message d'erreur
        				exit( 0 );
    				}
				}
			}


			image=chargement(inputfichier);				// chargement est la fonction qui permet de charger l'image




/*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -m et si oui
		            la fonction miroir est appliqué                                   */



			for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-m") ==0)
				{
					miroir(image);


				}
			}



/*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -p et si oui
		            la fonction rotation est appliqué                                   */			

			for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-p") ==0)
				{
					rotation(image);


				}
			}

			validation=0;

/*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -g et si oui
		       la fonction niveau de gris est appliqué                                   */

			
			for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-g") ==0)
				{
					   gris(image);
					   validation=1;

				}
			}


/*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -b et si 
		les arguments sont bien respecté alors la fonction binarisation 
		                       est appliqué                                               */

			
			for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-b") ==0)
				{
					   if (j+1!=argc)
					   {
					   	int valeur;
					   	valeur = atoi(tabchar[j+1]);
					   	binarisation(image,valeur);
					   	validation=1;

					   }
					   else
					   {
					   	printf("Erreur argument \n");
					   }

				}
			}


		int matrice[3][3];            //Création d'une matrice pour l'érosion et la dilatation 
		matrice[0][0]=0;
		matrice[0][1]=1;
		matrice[0][2]=0;
		matrice[1][0]=1;
		matrice[1][1]=1;
		matrice[1][2]=1;
		matrice[2][0]=0;
		matrice[2][1]=1;
		matrice[2][2]=0;




/*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -c aini
		la convolution est utilisé pour appliquer un filtre qui augmente 
								le contraste                                            */


	for (int j=0; j<argc ;j++)	
			{
				if (strcmp(tabchar[j], "-c") ==0)
				{
						float matrice2[9];
						matrice2[0]=0;
						matrice2[1]=-1;
						matrice2[2]=0;
						matrice2[3]=-1;
						matrice2[4]=5;
						matrice2[5]=-1;
						matrice2[6]=0;
						matrice2[7]=-1;
						matrice2[8]=0;
					   convolution(image,matrice2);

				}
			}



/*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -l aini
		la convolution est utilisé pour appliquer un filtre qui augmente 
								détecte les bords                                            */

	for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-l") ==0)
				{
						float matrice3[9];
						matrice3[0]=-1;
						matrice3[1]=-1;
						matrice3[2]=-1;
						matrice3[3]=-1;
						matrice3[4]=8;
						matrice3[5]=-1;
						matrice3[6]=-1;
						matrice3[7]=-1;
						matrice3[8]=-1;
					   convolution(image,matrice3);

				}
			}

  

  /*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -e et si 
		l'utilisateur a appliquer le filtre niveau de gris ou binarisation 
				alors la fonction érosion est appliqué a l'image                                   */


  for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-e") ==0)
				{
					if (validation==1)
					{
					   
					   	
    					erosion(image,matrice);
					   
					}
					else
					{
						printf("Pour utiliser la fonction erosion vous devez d'abbord appliquer le filtre 'niveau de gris' ou 'binarisation' \n");
					}



				}
			}
  /*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -d et si 
		l'utilisateur a appliquer le filtre niveau de gris ou binarisation 
				alors la fonction dilatation est appliqué a l'image                                   */	
	   
	   for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-d") ==0)
				{
					if (validation==1)
					{


					 
					   

					   dilatation(image,matrice);
					}
					else
					{
						printf("Pour utiliser la fonction dilatations vous devez d'abbord appliquer le filtre 'niveau de gris' ou 'binarisation' \n");
					}



				}
			}


  /*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -n et si 
		oui alors on va venir crée une image blanche avec une croix noire
					au millieu                                                                  */

		for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-n") ==0)
				{
					   

						int hauteur;
						int largeur;
						int epaisseur;
						printf("La largeur de votre croix\n");
						scanf("%d",&largeur);
						printf("La hauteur de votre croix\n");
						scanf("%d",&hauteur);
						printf("L'épaisseur de votre croix\n");
						scanf("%d",&epaisseur);

					   FILE* fichier;
  					   fichier = NULL;
  					    img newimage;
  						fichier = fopen("croix.ppm", "w");
  						rgb pixel;
  						pixel.r = 255;
  						pixel.g = 255;
  						pixel.b = 255;



  						newimage.nbLigne=256;
  						newimage.nbColonne=256;
  						newimage.tab= malloc(256 * sizeof (rgb*));
  						for (int i=0; i <256; i++)
  						{
    						newimage.tab[i]=  malloc(256 * sizeof (rgb));
  						}
  
  						fprintf (fichier, "P3\n256 256\n255\n");
  

  						for (int x=0; x < 256; x++)
  						{
    						for (int y=0; y < 256; y++)
    						{
      						newimage.tab[x][y] = pixel;
    						}
  						}
  						
  						creation_croix(newimage, hauteur, largeur, epaisseur);
  						sauvegarde(newimage,"imageCroix.ppm");

  						  fclose (fichier);

  						for (int i=0;i<256;i++)
  							{
    							free(newimage.tab[i]);
  							}
  						free(newimage.tab);
  						newimage.tab=NULL;

				}
			}

  /*   ---------------------------------------------------------------------------------
		Détection argument pour savoir si l'utilisateur a entré -o et si
		oui alors un fichier .ppm est enregistrer avec le nom voulue          */
			validation=0;
			for (int j=0; j<argc ;j++)
			{
				if (strcmp(tabchar[j], "-o") ==0)
				{
					   validation=1;
					   sauvegarde(image,tabchar[j+1]);
					   printf("Ne pas oublier de rentrer apres le -o un nom de fichier correct \n");

				}

			}



  /*   ---------------------------------------------------------------------------------
		Si l'utilisateur n'a pas entré l'argument -o alors le contenue du fichier
		                  est print dans le terminal      */
			
			if (validation==0)
			{
				for (int x=0;x<image.nbLigne;x++)
    				{
        				for (int y=0;y<image.nbColonne;y++)
        			{
            				affiche(image.tab[x][y]);



        			}
        			printf("\n");
    				}
			}
			fclose( inputfichier );
			free(image.tab);
			for (int i=0;i<image.nbLigne;i++)
			{
				free(image.tab[i]);
			}

		}

}
