#ifndef __FONCTION_H_
#define __FONCTION__H

/*creation de la structure d'un pixel constituer de la couleur rouge, bleu et vert.
Unsigned char parce que ca va de 0 a 255 */

struct rgb
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
};
typedef struct rgb rgb;

/*reation de la structure d'une image composé de son type, taille, la valeur maximun d'un pixel de 0 à 255,
et d'un tableau de pixel pour former l'image*/

struct img
{
    int type;
    int nbLigne;
    int nbColonne;
    int maxValPixel;
    rgb **tab;
};
typedef struct img img;

// listes des focntions utilisé dans le main

img chargement(FILE* inputfichier);              //signature de la fonction chargement
img sauvegarde(img image,char* nom);             //signature de la fonction sauvegarde
void gris(img image);                            //signature de la fonction gris
void miroir(img image);                          //signature de la fonction miroir
void rotation(img image);                        //signature de la fonction rotation
void binarisation(img image,int seuil);          //signature de la fonction binarisation
void affiche(rgb px);                            //signature de la fonction affiche
void negatif(img image);                         //signature de la fonction negatif
int** histo(img image);                          //signature de la fonction histo
void convolution(img image,float matrice[9]);    //signature de la fonction convolution
rgb* calcul(int x,int y,img image);              //signature de la fonction calcul
void erosion(img image, int matrice[3][3]);      //signature de la fonction erosion
void dilatation(img image, int matrice[3][3]);   //signature de la fonction dilatation
int croix(img image,int x,int y,int matrice[3][3],int validation);   //signature de la fonction croix
void creation_croix(img image,int hauteur,int largeur,int epaisseur);



#endif
