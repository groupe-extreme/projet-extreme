#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "fonction.h"
#define TAILLE_MAX 1000



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction chargement*/
/*Cette fonction permet de charger une image et de la mettre en Structure la taille de l'imge est stocké et
chaque paramètre de pixel est dans un tableau deux dimension de structure rgb qui prend en compte les 3 paramètre
d'un pixel*/


img chargement(FILE* inputfichier)
{
    img im;
    rgb px;
    char chaine[TAILLE_MAX]="";
    int actuel;
    fgets(chaine, TAILLE_MAX, inputfichier);
        actuel=atoi(chaine);
        im.type=actuel;
        fscanf(inputfichier, "%s", chaine);
        actuel=atoi(chaine);
        im.nbLigne=actuel;
        fgets(chaine, TAILLE_MAX, inputfichier);
        actuel=atoi(chaine);
        im.nbColonne=actuel;


        im.tab =malloc(sizeof(rgb*)*im.nbLigne);
        for (int i=0;i<im.nbLigne;i++){
            im.tab[i] = malloc (sizeof(rgb)*im.nbColonne);
        }

        int compt=1;
        int x=0;
        int y=0;
        fgets(chaine, TAILLE_MAX, inputfichier);
        while (fgets(chaine, TAILLE_MAX, inputfichier) != NULL) // On lit le fichier tant qu'on ne reçoit pas d'erreur (NULL)
        {
            if (compt==1)
            {
                px.r=actuel=atoi(chaine);
             }
            if (compt==2)
            {
                px.g=actuel=atoi(chaine);
            }
            if (compt==3)
            {
                px.b=actuel=atoi(chaine);
                im.tab[x][y]=px;
                y=y+1;
                compt=0;
            }

           if(y==im.nbColonne){
                x=x+1;
                y=0;



            }
           compt=compt+1;

           }

           return im;
}




/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction sauvegarde*/
/*fonction qui sauvegarde une image dans un fichier .ppm*/


img sauvegarde(img image, char* nom){
    img newimage;
    rgb px;
    FILE* fichier;;

    fichier=fopen(nom,"w");
    fprintf(fichier,"P3 \n");
    fprintf(fichier,"%d %d \n",image.nbLigne,image.nbColonne);
    fprintf(fichier,"255\n");
    for (int i=0;i<image.nbLigne;i++)
    {
        for (int j=0;j<image.nbColonne;j++)
        {
            px =image.tab[i][j];
            fprintf(fichier,"%hhu\n%hhu\n%hhu\n",px.r,px.g,px.b);
        }
    }
    fclose(fichier);
    return newimage;
}




/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction gris*/
/*fonction qui transforme une image coloré en une image en ivau de gris*/


void gris(img image)
{
    rgb px;
    int couleur;

    for (int x=0;x<image.nbLigne;x++)
    {
        for (int y=0;y<image.nbColonne;y++)
        {
            px=image.tab[x][y];
            couleur=0.2126*px.r + 0.7152*px.g + 0.0722*px.b;
            px.r=couleur;
            px.g=couleur;
            px.b=couleur;
            image.tab[x][y]=px;



        }
    }
}



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction miroir*/
/*fonction qui change une image effet miroir*/


void miroir(img image)
{
  rgb px;
  rgb px2;
  int ligne;
  int colonne;
  printf("ok");
  colonne=image.nbColonne;
  for (int x=0;x<image.nbLigne;x++)
  {
      for (int y=0;y<image.nbColonne/2;y++)
      {
          px=image.tab[x][y];
          px2=image.tab[x][colonne];
          image.tab[x][colonne]=px;
          image.tab[x][y]=px2;
          colonne=colonne-1;
        }
      colonne=image.nbColonne;
  }
}



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction rotation*/
/*fonction qui fai une rotation centrale de l'image*/


void rotation(img image)
{
  rgb px;
  int ligne;
  int colonne;
  img tamp;
  colonne=255;
  ligne=0;
  tamp.nbLigne=256;
  tamp.nbColonne=256;
  tamp.tab =malloc(sizeof(rgb*)*image.nbLigne);
  for (int i=0;i<image.nbLigne;i++){
      tamp.tab[i] = malloc (sizeof(rgb)*image.nbColonne);
  }
  for (int x=0;x<image.nbLigne-1;x++)
  {
      for (int y=0;y<image.nbColonne;y++)
      {

        tamp.tab[ligne][colonne]=image.tab[x][y];
        ligne=ligne+1;
        if (ligne==256)
        {
          ligne=0;
        }
        //printf("%d valeur de Ligne",ligne);
      }
        colonne=colonne-1;

  }
  for (int x=0;x<image.nbLigne-1;x++)
  {
      for (int y=0;y<image.nbColonne;y++)
      {
        image.tab[x][y]=tamp.tab[x][y];
      }
  }
}



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction binarisation*/
/*fonction qui passe d'une image coloré à une image à 2 couleurs : noir et blanc*/

void binarisation(img image,int seuil){
    rgb px;
    int couleur;

    for (int x=0;x<image.nbLigne;x++)
    {
        for (int y=0;y<image.nbColonne;y++)
        {
            px=image.tab[x][y];
            couleur= px.r + px.g +px.b;
            if (couleur<=seuil)
            {
                couleur=0;
            }
           else
           {
            couleur=255;
           }
           px.r=couleur;
           px.g=couleur;
           px.b=couleur;
           image.tab[x][y]=px;


        }
    }

}



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction negatif*/
/*fonction qui passe d'une image coloré à une image (coloré)^-1. Principe de physique : cercle chromatique*/

void negatif(img image)
{
    rgb px;
    int couleur;

    for (int x=0;x<image.nbLigne;x++)
    {
        for (int y=0;y<image.nbColonne;y++)
        {
            px=image.tab[x][y];
            couleur=(255- px.b);
            px.b=couleur;
            couleur=(255- px.g);
            px.g=couleur;
            couleur=(255- px.r);
            px.r=couleur;
            //px.g=couleur;
            //px.b=couleur;
            image.tab[x][y]=px;



        }
    }
}



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction histogramme*/
/*L’histogramme d’une image donne la fréquence d’apparition de chaque niveau de gris dans
l’image : luminance. C’est donc un tableau de 256 valeurs. Chacune des valeurs représente le nombre de fois que la luminance apparaît dans l’image*/


int** histo(img image)
{
    int **tab;
    rgb px;
    int luminance;
    int validation;
    validation=0;
    int colonne=0;
    int indice=0;
    tab =malloc(sizeof(int*)*image.nbColonne);
    for (int i=0;i<2;i++)
    {
      tab[i] = malloc (sizeof(int)*image.nbColonne);
    }
    
    for (int x=0;x<image.nbColonne; x++)
    {
        tab[1][x]=0;
    }
    
    for (int x=0;x<image.nbLigne;x++)
    {
        for (int y=0;y<image.nbColonne;y++)
        {
            px=image.tab[x][y];
            luminance=px.r;
            for (int j=0;j<image.nbColonne;j++)
            {
                if (tab[0][j]==luminance)
                {
                    validation=1;
                    indice=j;
                }
            }
            if (validation!=1)
            {
                tab[0][colonne]=luminance;
                colonne=colonne+1;

            }
            else
            {
                tab[1][indice]=tab[1][indice]+1;
            }



        }
    }
    int compt=0;
    for (int j=0;j<image.nbColonne;j++)
    {

        printf("%d",compt);
        printf("[%d-%d]\n",tab[0][j],tab[1][j]);
        compt=compt+1;
    }
    return tab;
  }



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction convolution*/
/*En traitement d'images, une matrice de convolution est une petite matrice utilisée pour le floutage, l'amélioration de la netteté de l'image, le gaufrage, la détection de contours, et d'autres*/


void convolution(img image,float matrice[9])
{

  rgb px;
  int red=0;
  int green=0;
  int blue=0;
  int valeur=0;
  rgb *tab;
  tab =malloc(sizeof(rgb)*9);
  img newimage;
  
  newimage.nbLigne=image.nbLigne;
  newimage.nbColonne=image.nbColonne;

  newimage.tab =malloc(sizeof(rgb*)*newimage.nbLigne);
  for (int i=0;i<newimage.nbLigne;i++){
      newimage.tab[i] = malloc (sizeof(rgb)*newimage.nbColonne);
  }

  for (int x=0;x<image.nbLigne;x++)
  {

    for(int y=0;y<image.nbColonne;y++)
    {
      tab=calcul(x,y,image);
      for (int i=0;i<9;i++)
      {
        px=tab[i];
        valeur=matrice[i];
        red= red + px.r* valeur;
        green=green + px.g * valeur;
        blue= blue+ px.b* valeur;
      }
      if (blue<0)
      {
        blue=blue*(-1);
      }
      if (green<0)
      {
       green=green*(-1);
      }
      if (red<0)
      {
        red=red*(-1);
      }
      px.r=red;
      px.g=green;
      px.b=blue;
      if (blue>255)
      {
        blue=blue;
      }
      if (green>255)
      {
        green=green;
      }
      if (red>255)
      {
        red=red;
      }
      newimage.tab[x][y]=px;
      red=0;
      green=0;
      blue=0;
  

}
  }


   for (int x=0;x<image.nbLigne;x++)
      {
        for (int y=0;y<image.nbColonne;y++)
          {
            image.tab[x][y]=newimage.tab[x][y];
          }


      }





}



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction calcul*/
/*fonction utilisé dans d'autres fonctions qui fait des calculs necessaire au bon fonctionnement des fonctions*/


rgb* calcul(int x,int y,img image)
{

  // La ligne 0 représente les coord x et la ligne 1 les coords y
  rgb *tab;
  rgb pxnul;
  rgb px;
  int valeury=y;
  int valeurx=x;
  int outxh=0;   //xh signifie out en haut
  int outyd=0;  //yd signifie out a droite
  int outxb=0;
  int outyg=0;  //etc

  pxnul.r=0;
  pxnul.b=0;
  pxnul.g=0;
  valeury=valeury-1;
  tab =malloc(sizeof(int)*9);
  if (valeury<0)
    {
      outyg=1;
    }
  valeurx=valeurx-1;
  if (valeurx<0)
    {
      outxh=1;
    }
  valeury=y;
  valeury=valeury+1;
  if (valeury>=image.nbColonne)
    {
      outyd=1;
    }
  valeurx=x;
  valeurx=valeurx+1;
  if (valeurx>=image.nbLigne)
    {
      outxb=1;
    }

  



  if (outxh==1)
  {
    tab[0]=pxnul;
    tab[1]=pxnul;
    tab[2]=pxnul;

   if (outyg==1)
      {

        tab[3]=pxnul;
        tab[6]=pxnul;
        tab[4]=image.tab[x][y];
        tab[5]=image.tab[x][y+1];
        tab[7]=image.tab[x+1][y];
        tab[8]=image.tab[x+1][y+1];
      }
   else
      {
        tab[3]=image.tab[x][y-1];
        tab[4]=image.tab[x][y];
        tab[6]=image.tab[x+1][y-1];
        tab[7]=image.tab[x+1][y];
        if (outyd==1)
          {
            tab[5]=pxnul;
            tab[8]=pxnul;
          }
        else
          {
            tab[5]=image.tab[x][y+1];
            tab[8]=image.tab[x+1][y+1];
          }
      }
  }
  else
  {
    if (outyg==1)
    {
      tab[0]=pxnul;
      tab[3]=pxnul;
      tab[6]=pxnul;
      valeurx=x-1;
      valeury=y;
      tab[1]=image.tab[x-1][y];
      tab[2]=image.tab[valeurx][y+1];
      tab[4]=image.tab[x][y];
      tab[5]=image.tab[x][y+1];
      if (outxb==1)
        {
          tab[7]=pxnul;
          tab[8]=pxnul;
        }
      else
        {
          tab[7]=image.tab[x+1][y-1];
          tab[8]=image.tab[x+1][y];
        }
    }
    else
    {
      tab[0]=image.tab[x-1][y-1];
      tab[1]=image.tab[x-1][y];
      tab[3]=image.tab[x][y-1];
      tab[4]=image.tab[x][y];
      if (outyd==1)
      {
        tab[2]=pxnul;
        tab[5]=pxnul;
        tab[8]=pxnul;
        if (outxb==1)
        {
          tab[6]=pxnul;
          tab[7]=pxnul;
        }
        else
        {
          tab[6]=image.tab[x+1][y-1];
          tab[7]=image.tab[x+1][y];
        }
      }
      else
      {
        tab[2]=image.tab[x-1][y+1];
        tab[5]=image.tab[x][y+1];
        if (outxb==1)
        {
          tab[6]=pxnul;
          tab[7]=pxnul;
          tab[8]=pxnul;
        }
        else
        {
          tab[6]=image.tab[x+1][y-1];
          tab[7]=image.tab[x+1][y];
          tab[8]=image.tab[x+1][y+1];
        }

      }
    }

  }


  return tab;
}
  


/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction erosion */
/*inverse de la dilatation*/


void erosion(img image, int matrice[3][3])
{
  int min;
  rgb px;
  rgb pxblanc;
  img newimage;

  pxblanc.r=255;
  pxblanc.g=255;
  pxblanc.b=255;

  newimage.nbLigne=image.nbLigne+2;
  newimage.nbColonne=image.nbColonne+2;

  newimage.tab =malloc(sizeof(rgb*)*newimage.nbLigne);
  for (int i=0;i<newimage.nbLigne;i++){
      newimage.tab[i] = malloc (sizeof(rgb)*newimage.nbColonne);
  }
  
  for (int y=0;y<newimage.nbColonne;y++)
      {
       newimage.tab[0][y]=pxblanc;
      }
  for (int y=0;y<newimage.nbColonne;y++)
      {
       newimage.tab[257][y]=pxblanc;
      }
  for (int x=0;x<newimage.nbLigne;x++)
      {
       newimage.tab[x][0]=pxblanc;
      }
  for (int x=0;x<newimage.nbLigne;x++)
      {
       newimage.tab[x][257]=pxblanc;
      }


  for (int x=0;x<image.nbLigne;x++)
      {
        for (int y=0;y<image.nbColonne;y++)
          {
            //image.tab[x][y]=newimage.tab[x+1][y+1];
            newimage.tab[x+1][y+1]=image.tab[x][y];
          }


      }
  for (int x=0;x<image.nbLigne;x++)
      {
        for (int y=0;y<image.nbColonne;y++)
          {
            //image.tab[x][y]=newimage.tab[x+1][y+1];
            newimage.tab[x+1][y+1]=image.tab[x][y];
          }


      }




   for (int x=0;x<image.nbLigne;x++)
    {

      for (int y=0;y<image.nbColonne;y++)
        {
         min = croix(newimage,x+1,y+1,matrice,0);
         px.r=min;
         px.g=min;
         px.b=min;
         image.tab[x][y]=px;
        }
    }
}




/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction croix */ 


int croix(img image,int x,int y,int matrice[3][3],int validation)
{
    int tab[3][3];
    rgb px;
    int valeurx=x-1;
    int valeury=y-1;

    for (int i=0;i<3;i++)
    {

      for (int j=0;j<3;j++)
        {
          px=image.tab[valeurx][valeury];
          tab[i][j]=px.r;
          valeury=valeury+1;
        }
      valeurx=valeurx+1;
      valeury=y-1;

    }

     for (int i=0;i<3;i++)
    {

      for (int j=0;j<3;j++)
        {
          if (matrice[i][j]==1)
          {
            tab[i][j]=tab[i][j];
          }
          else
          {
            if (validation==0)
            {
                tab[i][j]=300;
            }
            else
            {
               tab[i][j]=0;
            }
          }
        }

    }


    if (validation==0)
    {

      int min;
    min=300;
    for (int i=0;i<3;i++)
    {

      for (int j=0;j<3;j++)
        {
          if (min>tab[i][j])
          {
            min=tab[i][j];
          }
        }

    }
    return min;

    }
    else
    {
     // printf("Ok on passe \n");
      int max;
      max=0;
      for (int i=0;i<3;i++)
        {

          for (int j=0;j<3;j++)
           {
             if (max<tab[i][j])
             {
               max=tab[i][j];
              }
        }
    }
    return max;
    }
}



/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation de la fonction dilatation*/
/*inverse de l'erosion*/


void dilatation(img image, int matrice[3][3])
{
  {
  int max;
  rgb px;
  rgb pxblanc;
  img newimage;

  pxblanc.r=0;
  pxblanc.g=0;
  pxblanc.b=0;

  newimage.nbLigne=image.nbLigne+2;
  newimage.nbColonne=image.nbColonne+2;

  newimage.tab =malloc(sizeof(rgb*)*newimage.nbLigne);
  for (int i=0;i<newimage.nbLigne;i++){
      newimage.tab[i] = malloc (sizeof(rgb)*newimage.nbColonne);
  }
  
  for (int y=0;y<newimage.nbColonne;y++)
      {
       newimage.tab[0][y]=pxblanc;
      }
  for (int y=0;y<newimage.nbColonne;y++)
      {
       newimage.tab[257][y]=pxblanc;
      }
  for (int x=0;x<newimage.nbLigne;x++)
      {
       newimage.tab[x][0]=pxblanc;
      }
  for (int x=0;x<newimage.nbLigne;x++)
      {
       newimage.tab[x][257]=pxblanc;
      }


  for (int x=0;x<image.nbLigne;x++)
      {
        for (int y=0;y<image.nbColonne;y++)
          {
            //image.tab[x][y]=newimage.tab[x+1][y+1];
            newimage.tab[x+1][y+1]=image.tab[x][y];
          }


      }





   for (int x=0;x<image.nbLigne;x++)
    {

      for (int y=0;y<image.nbColonne;y++)
        {
         max = croix(newimage,x+1,y+1,matrice,1);
         px.r=max;
         px.g=max;
         px.b=max;
         image.tab[x][y]=px;

        }


    }
}

}


void creation_croix(img image,int hauteur,int largeur,int epaisseur)
{
  rgb px;
  int targety;
  int departy;
  int targetx;
  int departx;


  px.r=0;
  px.g=0;
  px.b=0;

  departy=(image.nbColonne/2)-(largeur/2);
  targety=(image.nbColonne/2)+(largeur/2);
  departx=(image.nbLigne/2)-(epaisseur/2);
  targetx=(image.nbLigne/2)+(epaisseur/2);

  for (int x=departx;x<targetx;x++)
  {
    for (int y=departy;y<targety;y++)
    {
      image.tab[x][y]=px;
    }
  }

  departy=(image.nbColonne/2)-(epaisseur/2);
  targety=(image.nbColonne/2)+(epaisseur/2);
  departx=(image.nbLigne/2)-(hauteur/2);
  targetx=(image.nbLigne/2)+(hauteur/2);


  for (int y=departy;y<targety;y++)
  {
    for (int x=departx;x<targetx;x++)
    {
      image.tab[x][y]=px;
    }
  }


}

/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*creation fonction affche*/

void affiche(rgb px){
    printf("|%hhu|\n",px.r);
    printf("|%hhu|\n",px.g);
    printf("|%hhu|\n \n",px.b);
}
